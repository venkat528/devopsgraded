package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Books;
import com.service.BooksService;



@RestController
@RequestMapping(value="book")
public class BooksController {
	@Autowired
	BooksService booksService;
	
	@PostMapping(value="store",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String storeBookInfo(@RequestBody Books book) {
		return booksService.storeBookDetails(book);
	}
	
	@PatchMapping(value="update")
	public String updateBookInfo(@RequestBody Books book) {
		return booksService.updateBook(book);
	}
	
	@GetMapping(value="Details")
	public List<Books> getAllBooksAvaliable() {
		return booksService.getAllBooksAvaliable();
	}
	
	@GetMapping(value="findById/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public Books fingBookById(@PathVariable("id") int id) {
		return booksService.findBooksById(id);
	}
	
	@DeleteMapping(value="deleteById/{id}")
	public String deleteBookById(@PathVariable("id") int id) {
		return booksService.deleteBookById(id);
	}
}
