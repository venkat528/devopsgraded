package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Users;
import com.dao.UsersDao;

@Service
public class UsersService {
	@Autowired
	UsersDao usersDao;
	
	//Store User Details
	public String storeUserDetails(Users user) {
		if(!usersDao.existsById(user.getEmail())) {
			usersDao.save(user);
			return "user your details saved sucessfully";
		}else {
			return "your details are already present";
		}
	}
	
	//Update User Details ByEmail
	public String updateUserDetails(Users user) {
		if(usersDao.existsById(user.getEmail())) {
			Users u=usersDao.getById(user.getEmail());
			if(u.getPassword().equals(user.getPassword())) {
				return "You are providing the old input";
			}else {
				u.setPassword(user.getPassword());
				usersDao.saveAndFlush(u);
				return "your details are updated sucessfully";
			}
		}else {
			return "your details are not present";			
		}						
	}
	
	//ToSee All Users Information
	public List<Users> getAllUsersDetails() {
		return usersDao.findAll();
	}

	//To delete a userBy Email
	public String deleteUserDetails(String email) {
		if(!usersDao.existsById(email)) {
			return "your details are not present";
		}else {
			usersDao.deleteById(email);
			return "user Details Deleted sucessfully";
		}
	}
	
}
